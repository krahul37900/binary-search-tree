
public class BSTTree {

	public static void main(String [] args)
	{
		BST tree = new BST();
		int [] a = {10,5,20,1,8,19,100,41,242,17,4,0,7,9,21};
		
		for(Integer i : a)
		{
			tree.insert(i);
		}
		
		tree.displayTreeFormat();
	}
}
