import java.util.LinkedList;
import java.util.Queue;


public class BSTNode {

	int number;
	BSTNode left;
	BSTNode right;
	
	/**
	 * Construct a new Tree Node
	 * @param element
	 */
	public BSTNode(int element)
	{
		this.number = element;
		this.left = null;
		this.right = null;
	}
	
	/**
	 * Dispaly the elements in the BST
	 */
	public void display()
	{
		System.out.println(this.number);
		if(null != this.left)
			this.left.display();
		if(null != this.right)
			this.right.display();
	}
	
	/**
	 * Display in actual tree structure.
	 */
	public void displayInTreeFormat()
	{
		Queue<BSTNode> queue = new LinkedList<BSTNode>();
		queue.add(this);
		System.out.println(this.number);
		
		int level = 1;
		int numElePrinted = 0;
		
		while(!queue.isEmpty())
		{
			
			BSTNode node = queue.remove();
			if(node.left != null)
			{
				System.out.print(node.left.number + " ");
				numElePrinted++;
				queue.add(node.left);
			}
			if(node.right != null)
			{
				System.out.print(node.right.number + " ");
				numElePrinted++;
				queue.add(node.right);
			}
			
			if(numElePrinted >= (1<<level))
			{
				System.out.println();
				numElePrinted = 0;
				level++;
			}
			
		} // end while
	}
	
	/**
	 * Insert the given element in the BST.
	 * @param element
	 */
	public void insert(int element)
	{
		if(this.number > element)
		{
			if(null == this.left)
			{
				this.left = new BSTNode(element);
			}
			else {
				this.left.insert(element);
			}		
		}
		else {
			if(null == this.right)
			{
				this.right = new BSTNode(element);
			}
			else {
				this.right.insert(element);
			}
		}
	}
}
