
public class BST {
	BSTNode root;
	
	/**
	 * Construct a new BST
	 */
	public BST()
	{
		this.root = null;
	}
	
	/**
	 * Insert the given element in the BST
	 * @param element
	 */
	public void insert(int element)
	{
		if(null == this.root)
		{
			this.root = new BSTNode(element);
		}
		else {
			this.root.insert(element);
		}
	}
	
	/**
	 * Display in Tree Format
	 */
	public void displayTreeFormat()
	{
		if(null != this.root)
			this.root.displayInTreeFormat();
		else
			System.out.println("Tree is empty");
	}
	
	/**
	 * Display the binary search tree contents.
	 */
	public void display()
	{
		if( null != this.root)
			this.root.display();
		else
			System.out.println("Tree is empty");
	}
}
